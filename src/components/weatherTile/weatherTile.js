import React from "react";

function WeatherTile({ ...props }) {

    const icon = (props.weather.icon) ? <img src={props.weather.icon} className="weather-icon" alt="Cloudy" />
                                        : '';

    const wind = (props.windStatus === true) ?
        <div className="weather clearboth">
            <span>Wind</span>
            <span>{props.weather.wind.deg} {props.weather.wind.speed}</span>
        </div>
        : '';

    return (
        <div className="tile">
            <h1>{props.title}</h1>
            <div className="wflex tile-content">
                <div>
                     {icon}                
                </div>
                <div>
                    <div className="city">{props.weather.city}</div>
                    <div className="degree">
                        <div>{props.weather.temp}</div><div>&#186;</div>
                    </div>
                    {wind}
                </div>
            </div>
        </div>
    );
}

export default WeatherTile;