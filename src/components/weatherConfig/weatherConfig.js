import React from "react";

function WeatherConfig({ ...props }) {
    
    return (
        <div className="config">
            <form>
                <div className="w-row">
                    <label className="w-label">Title</label>
                    <input type="text" 
                        id="wtitle" 
                        name="wtitle"
                        className="w-control w-textbox"
                        onChange={(e) => {props.changeTitle(e.target.value)}}
                        placeholder={props.defaultTitle} />
                </div>
                <div className="w-row">
                    <label className="w-label">Temperature</label>
                    <div className="w-control">
                        <div className="w-control-section w-control-gap1">
                            <label className="container"><span>&#186;</span>C
                                <input type="radio"
                                    id="Temperature1" name="Temperature"
                                    checked={props.temperatureType === 'c'}
                                    onChange={() => {props.changeTemperature('c')}} />
                                <span className="checkmark"></span>
                            </label>
                        </div>

                        <div className="w-control-section">
                            <label className="container"><span>&#186;</span>F
                                <input type="radio"
                                    id="Temperature2" name="Temperature"
                                    checked={props.temperatureType === 'f'}
                                    onChange={() => {props.changeTemperature('f')}} />
                                <span className="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    
                </div>    

                <div className="w-row clearboth">
                    <label className="w-label">Wind</label>
                    <div className="w-control">
                        <div className="w-control-section w-control-gap2">
                            <label className="container">
                                <span>On</span>
                                <input type="radio"
                                    id="Wind1" name="Wind" 
                                    checked={props.windStatus === true}
                                    onChange={() => {props.changeWind(true)}} />
                                <span className="checkmark"></span>
                            </label>
                        </div>

                        <div className="w-control-section">
                            <label className="container">
                                <span>Off</span>
                                <input type="radio"
                                    id="Wind2" name="Wind" 
                                    checked={props.windStatus === false}
                                    onChange={() => {props.changeWind(false)}} />
                                <span className="checkmark"></span>
                            </label>
                        </div>
                    </div>
                </div>  
            </form>
        </div>
    );
}

export default WeatherConfig;