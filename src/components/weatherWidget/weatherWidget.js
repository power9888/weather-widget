import React, { Component } from 'react';
import '../../assets/css/radio-style.css';
import './weatherWidget.css';
import WeatherConfig from '../weatherConfig/weatherConfig';
import WeatherTile from '../weatherTile/weatherTile';
import axios from 'axios';

class WeatherWidget extends Component {

    constructor(props) {
        super(props);

        //do not have time to do redux. If this is a large app, redux will be added to manage state.
        this.state = {
            defaultTitle: 'TITLE OF WIDGET',
            title: 'TITLE OF WIDGET',
            temperatureType: 'c',
            windStatus: true,
            weather: {
                city: '',
                weatherDescription: '',
                icon: '',
                temp: '',
                wind: {
                    deg: '',
                    speed: '',
                },
            }
        };

        this.success = this.success.bind(this);

        this.getWeather();
    }

    getWeather() {
        //get location then get weather by location lat and log
        navigator.geolocation.getCurrentPosition(this.success, this.error, {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
          });
    }

    //get location success
    success(pos) {
        var crd = pos.coords;
        var key = '4b308718444050cb581acad9bbe25a18';

        var self = this;
        var url = `https://api.openweathermap.org/data/2.5/weather?lat=${crd.latitude}&lon=${crd.longitude}&appid=${key}`;        

        if (this.state.temperatureType === 'c') {
            url += '&units=metric';
        }
        else if (this.state.temperatureType === 'f') {            
            url += '&units=imperial';
        }
        else {
            console.log('Please select temperature type');
            return;
        }

        axios.get(url)
        .then(response => {

            //load weather data to state
            var data = response.data;
            var weather = self.state.weather;
            weather.city = data.name;
            weather.weatherDescription = data.weather.description;
            
            if (data.weather && data.weather.length > 0) {
                weather.icon =  `http://openweathermap.org/img/w/${data.weather[0].icon}.png`;
            }
            else {
                weather.icon = '';
            }
            weather.wind.deg = data.wind.deg;
            weather.wind.speed = `${data.wind.speed}km/h`;
            weather.temp = isNaN(data.main.temp) ? '' : parseInt(data.main.temp);
            self.setState({weather: weather});
        })
        .catch(function (error) {
            console.log('error', error);
            //exception message can be shown to the screen. As this is not in the requirement, ignore it.
        }); 
    }

    //get location error
    error(err) {
        console.warn(`ERROR(${err.code}): ${err.message}`);
    }

    changeTitle(text) {
        if (text) {
            this.setState({title: text});
        }
        else {
            this.setState({title: this.state.defaultTitle});
        }        
    }

    changeTemperature(type) {
        switch(type) {
            case 'c':
                this.setState({temperatureType: 'c'});
                this.getWeather();
                break;
            case 'f':
                this.setState({temperatureType: 'f'});
                this.getWeather();
                break;
            default:
                this.setState({temperatureType: ''});
                break;
        }
    }

    changeWind(wstatus) {
        switch(wstatus) {
            case wstatus === true:
                this.setState({windStatus: wstatus});
                break;
            case wstatus === false:
                this.setState({windStatus: wstatus});
                break;
            default:
                this.setState({windStatus: ''});
                break;
        }
    }

    render() {
        return (
            <div className="weather-widget">
                <hr/>

                <div className="wflex wbox bg-grey">                
                    <div className="section left-side seperator">                        
                        <WeatherConfig
                            defaultTitle={this.state.defaultTitle}
                            changeTitle={this.changeTitle.bind(this)}
                            changeTemperature={this.changeTemperature.bind(this)}
                            changeWind={this.changeWind.bind(this)}
                            windStatus={this.state.windStatus}
                            temperatureType={this.state.temperatureType}>
                        </WeatherConfig>
                    </div>
                    <div className="section right-side">
                        <WeatherTile 
                            title={this.state.title}
                            windStatus={this.state.windStatus}
                            temperatureType={this.state.temperatureType}
                            weather={this.state.weather}>
                        </WeatherTile>                            
                    </div>
                </div>
            </div>
        );
    }
}

export default WeatherWidget;